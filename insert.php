<?php
$servername = "mysqldb";
$database = "viajes";
$username = "root";
$password = "tudai";

$nombre = "Emilio";
$edad = "50";

try {
    // Creo la conexion:
    $conn = new PDO("mysql:host=$servername;dbname=$database", $username, $password);
    // Armo mi sentencia SQL
    $sql = 'INSERT INTO conductor (nombre, edad)
            VALUES (:nombre, :edad)';

    // Preparo la sentencia:
    $sth = $conn->prepare($sql);
    // Ejecuto la sentencia
    $sth->execute([':nombre'=>$nombre, ':edad'=> $edad]);
    
    // Recupero el resultado
    $resultado = $sth->fetchAll();   
    // Muestro el resultado
    print_r($resultado);

  } catch(PDOException $e) {
    echo "Connection failed: " . $e->getMessage();
  }