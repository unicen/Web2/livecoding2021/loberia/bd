<?php

    include_once("connect.php");
    include_once("config.php");

try {
    $conn = getConnect($servername, $database,  $username, $password);
    // Armo mi sentencia SQL
    $sql = 'SELECT *
            FROM conductor';
    // Preparo la sentencia:
    $sth = $conn->prepare($sql);
    // Ejecuto la sentencia
    $sth->execute();
    // Recupero el resultado
    $resultado = $sth->fetchAll();   
    // Muestro el resultado
    print_r($resultado);

  } catch(PDOException $e) {
    echo "Connection failed: " . $e->getMessage();
  }