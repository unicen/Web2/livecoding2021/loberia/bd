<?php
    include_once("connect.php");
    include_once("config.php");


try {
    // Creo la conexion:
    $conn = getConnect($servername, $database,  $username, $password);
    // Armo mi sentencia SQL
    $sql = 'SELECT *
            FROM conductor';
    // Preparo la sentencia:
    $sth = $conn->prepare($sql);
    // Ejecuto la sentencia
    $sth->execute();
    // Recupero el resultado
    $resultado = $sth->fetchAll();   

    echo("<table border=1>");
    foreach ($resultado as $fila) {
      echo("<tr>");
      echo("<td>"); echo($fila['nombre']); echo("</td>");
      
      echo("<td>"); echo($fila['edad']); echo("</td>");
      echo("</tr>");
    }
    echo("</table>");

  } catch(PDOException $e) {
    echo "Connection failed: " . $e->getMessage();
  }